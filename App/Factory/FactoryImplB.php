<?php

namespace AbstractFactory\App\Factory;
use AbstractFactory\App\Plugin\AbstractPlugin;
use AbstractFactory\App\Plugin\PluginImplB;

class FactoryImplB implements AbstractFactory
{

    public function getPluginInstance(): AbstractPlugin
    {
        return new PluginImplB();
    }
}