<?php

namespace AbstractFactory\App\Factory;
use AbstractFactory\App\Plugin\AbstractPlugin;

interface AbstractFactory
{
    public function getPluginInstance(): AbstractPlugin;
}