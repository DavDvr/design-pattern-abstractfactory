<?php

namespace AbstractFactory\App\Factory;
use AbstractFactory\App\Plugin\AbstractPlugin;
use AbstractFactory\App\Plugin\PluginImplA;

class FactoryImplA implements AbstractFactory
{

    public function getPluginInstance(): AbstractPlugin
    {
        return new PluginImplA();
    }
}