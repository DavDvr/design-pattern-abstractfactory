<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home Factory</title>
</head>
<body>
<?php


use AbstractFactory\App\Factory\AbstractFactory;
use AbstractFactory\App\Factory\FactoryImplB;
use AbstractFactory\App\Plugin\AbstractPlugin;

require (dirname(__DIR__). '/vendor/autoload.php');

//instanciation statique
    $factory = AbstractFactory::class;
    $factory = new FactoryImplB() ;

    $plugin = AbstractPlugin::class;
    $plugin = $factory->getPluginInstance();
    $plugin->traitement();

//instanciation dynamique en utilisant ReflectionClass
    $factory2 = AbstractFactory::class;
    $factory2 = new ReflectionClass('AbstractFactory\App\Factory\FactoryImplA');
    $plugin2 = $factory2->newInstance();
    $plugin2->getPluginInstance()->traitement();





?>

</body>
</html>
