<?php

namespace AbstractFactory\App\Plugin;

interface AbstractPlugin
{
    public function traitement():void;
}