<?php

namespace AbstractFactory\App\Plugin;

class PluginImplB implements AbstractPlugin
{

    public function traitement(): void
    {
        echo "Traitement du plugin B<br>";
    }
}