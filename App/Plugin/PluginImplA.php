<?php

namespace AbstractFactory\App\Plugin;

class PluginImplA implements AbstractPlugin
{

    public function traitement(): void
    {
        echo "Traitement du plugin A<br>";
    }
}